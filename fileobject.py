"""Tools for working with files."""

import os
import re
import shutil
from bs4 import BeautifulSoup


class FileObject():

    """Initiate file processes."""

    def __init__(self):
        """Nothing."""
        pass

    def copy_all_files(self, source_dir, target_dir, file_ending=""):
        """Copy files from source to target.

        Copy all files from a particular directory (traversing all
            subdirectories), optionally specifying a file ending.

        args:
            source_dir (str) -- directory to traverse
            target_dir (str) -- directory to write files to (in flat structure)

        kwargs:
            file_ending (str) -- file ending of files to copy -- defaults to
                all files.
        """
        self.source_dir = source_dir
        self.target_dir = target_dir
        self.file_ending = file_ending
        self.file_list = []
        self.__get_files_in_dir()

    def __get_files_in_dir(self):
        """Store all files from source_dir in generator."""
        for dirpath, dirs, files in os.walk(self.source_dir):
            for f in files:
                if f.endswith(self.file_ending):
                    self.file_list.append(os.path.join(dirpath, f))
        self.__copy_files()

    def __copy_files(self):
        """Copy files to target directory."""
        for f in self.file_list:
            filename = os.path.basename(f)
            shutil.copyfile(f, os.path.join(self.target_dir, filename))

        print "Copied {0} files".format(len(self.file_list))

    def fix_image_duplicates(self, file_dir):
        """Delete duplicate derivatives.

        Respond to particular imagemagick error -- eliminate duplicate
            derivatives.

        args:
            file_dir (str) -- directory in which to make file changes.
        """
        endings = [".jpg", ".tif", ".jp2"]
        for f in os.listdir(file_dir):
            if any([f.endswith(e) for e in endings]):
                if os.path.splitext(f)[0].endswith("-0"):
                    file_path = os.path.join(file_dir, f)
                    os.remove(file_path)

                elif os.path.splitext(f)[0].endswith("-1"):
                    file_path = os.path.join(file_dir, f)
                    os.rename(file_path, file_path.replace("-1", ""))

    def allocate_metadata(self, metadata_dir, target_dir, file_ending,
                          exclude_string="*", include_string=""):
        """Move files to ingest directory.

        args:
            meatadata_dir(str): dir to tranverse to find ingest files.
            target_dir(str): location to store ingest-ready files.
            file_ending(str): ending to match when gathering files.
        kwargs:
            exlcude_string(str): exclude operations on filepaths that include
                this string.
            include_string(str): string that must match in path to proceed with
                operation.
        """
        self.metadata_dir = metadata_dir
        self.target_dir = target_dir
        self.exclude_string = exclude_string
        self.include_string = include_string
        self.file_ending = file_ending
        self._check_for_metadata()

    def _check_for_metadata(self):
        """Check if metadata files for given object exist."""
        for path, basename in self._gather_files():
            for f in os.listdir(self.metadata_dir):
                if basename in f:
                    filename = os.path.join(self.metadata_dir, f)
                    self._move_file(filename, path)
            else:
                pass
                # print basename, " ***NOT FOUND***"

    def _gather_files(self, file_dir, exlude_string="*", file_ending=""):
        """Yield paths and filenames for matching files.

        Use specified file ending and 'exclude string' to match files for
            processing.

        yields:
            (tuple): 3-tuple of path to file, filename w/o extension, extension.
        """
        for root, dirs, files in os.walk(file_dir):
            for f in files:
                if self.exclude_string not in root.lower()\
                   and f.endswith(file_ending)\
                   and self.include_string in root.lower():
                    yield root, os.path.splitext(f)[0], os.path.splitext(f)[1]

    def _move_file(self, filename, target_path):
        """Move file to target.

        args:
            filename(str): full path of file to copy.
            target_path(str): full path of directory to copy to.
        """
        shutil.copy(filename, target_path)
        print "{0} copied to {1}".format(filename, target_path)

    def copy_plain_text_from_gale(self, source_dir, replace_str=("XML", "TXT")):
        """Extract plain text and copy to new file.

        args:
            source_dir(str): location of marked up text.
        kwargs:
            replace_str(str): path manipulation to store plain-text in new field.
        """
        count = 0
        for path, dirs, files in os.walk(source_dir):
            count += 1
            if count % 100 == 0:
                print "Processed {0} files".format(count)
            for filename in files:
                if filename.endswith(".xml"):
                    input_filepath = os.path.join(path, filename)
                    output_filename = filename.replace(".xml", ".txt")
                    output_path = path.replace(replace_str[0], replace_str[1])
                    if not os.path.exists(output_path):
                        os.makedirs(output_path)
                    output_filepath = os.path.join(output_path, output_filename)

                    with open(output_filepath, "w") as f:
                        soup = BeautifulSoup(open(input_filepath))
                        bodytext = soup.find_all("p")
                        full_text = u''
                        for paragraph in bodytext:
                            paragraph_text = u''
                            wordtext = paragraph.find_all("wd")
                            for word in wordtext:
                                paragraph_text += word.get_text() + u' '
                            full_text += paragraph_text + u'\n\n'
                        f.write(full_text.encode("utf-8"))
        print "Complete. Processed {0} files".format(count)

    def arrange_dmhsp(self, file_dir, target_dir, exclude_string="*", include_string=""):
        """Rearrange dmhsp into standard dir structure.

        args:
            filedir(str): location of files.
            targetdir(str): where to put newly arranged files.
        kwargs:
            exclude_string
            include_string
        """
        self.target_dir = target_dir
        self.exclude_string = exclude_string
        self.include_string = include_string
        for path, filename, ext in self._gather_files(file_dir, file_ending=".xml"):
            if self._get_site_seq(filename):
                if len(self._seq) == 1:
                    self._seq = "0" + self._seq
                new_path = os.path.join(target_dir, self._site, self._site + self._seq)
                if not os.path.exists(new_path):
                    os.makedirs(new_path)
                source_file = os.path.join(path, filename + ext)
                self._move_file(source_file, new_path)
            else:
                print "{0} failed".format(filename)

    def _get_site_seq(self, string):
        """Pluck site and sequence from appropriately formatted filename string.

        args:
            string(str): String that should follow the pattern
            <sitename><sequencee)__dc, for ex.
        """
        pattern = r'(?P<site>[A-Za-z]*)(?P<sequence>[0-9]{1,2})'
        match = re.match(pattern, string)
        if match:
            self._site = match.group("site")
            self._seq = match.group("sequence")
            return True
        else:
            return False
