# -*- coding: utf-8 -*-

import json
import codecs
from dicttoxml import dicttoxml
import os
from datetime import datetime

class FormatObject():

    def __init__(self):
        pass

    def SaveAsJson(self, data, filepath):
        with open(filepath, "w") as f:
            json.dump(data, f)

    def LoadJson(self, filepath):
        with open(filepath) as f:
            data = json.load(f)
        return data

    def DictToXml(self, dictionary):
        xml = dicttoxml(dictionary)
        return xml

    def WriteFile(self, data, path, opener="w"):
        with open(path, opener) as f:
            f.write(data)

    def WriteJsonFromFiles(self, input_path):
        """
        Should be a text file with 1 item in line, saved as list
        """
        output = []
        with open(path, "r") as f:
            for line in f:
                output.append(line)

        self.SaveAsJson(output, input_path.replace(".txt", ".json"))

    def make_results_tsv(self, data, process="process", output_file=None):
        """
        Turn Python list of dictionaries into a TSV file.

        kwargs:
        process(str) -- description of the process that was run, in order to include it in default filename, if desired.
        output_file(str) -- provide file location for output, if none supplied, defaults to dated generic filename.
        """

        if not output_file:
            output_file = os.path.join("output", "{0}_results_{1}.txt".format(process, datetime.now().strftime("%Y-%m-%d-%H%M")))
        
        with codecs.open(output_file, "w", "utf-8") as fh:
            fh.write("\t".join(data[0].keys()) + "\n")
            for row in data:
                line = "\t".join([row[i] for i in row])
                fh.write(line)
                fh.write("\n")

        print "Wrote file at {0}".format(output_file)


        